﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Counteur.Razc();
            Assert.AreEqual(0, Counteur.affichageC());
            Counteur.increC();
            Assert.AreEqual(1,Counteur.affichageC());
            Counteur.decreC();
            Assert.AreEqual(0, Counteur.affichageC());
        }
    }
}
