﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.reduce = New System.Windows.Forms.Button()
        Me.add = New System.Windows.Forms.Button()
        Me.miseazero = New System.Windows.Forms.Button()
        Me.Total = New System.Windows.Forms.Label()
        Me.Valeur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'reduce
        '
        Me.reduce.Location = New System.Drawing.Point(187, 220)
        Me.reduce.Name = "reduce"
        Me.reduce.Size = New System.Drawing.Size(75, 23)
        Me.reduce.TabIndex = 0
        Me.reduce.Text = "-"
        Me.reduce.UseVisualStyleBackColor = True
        '
        'add
        '
        Me.add.Location = New System.Drawing.Point(488, 215)
        Me.add.Name = "add"
        Me.add.Size = New System.Drawing.Size(75, 23)
        Me.add.TabIndex = 1
        Me.add.Text = "+"
        Me.add.UseVisualStyleBackColor = True
        '
        'miseazero
        '
        Me.miseazero.Location = New System.Drawing.Point(328, 274)
        Me.miseazero.Name = "miseazero"
        Me.miseazero.Size = New System.Drawing.Size(75, 23)
        Me.miseazero.TabIndex = 2
        Me.miseazero.Text = "RAZ"
        Me.miseazero.UseVisualStyleBackColor = True
        '
        'Total
        '
        Me.Total.AutoSize = True
        Me.Total.Location = New System.Drawing.Point(347, 185)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(31, 13)
        Me.Total.TabIndex = 3
        Me.Total.Text = "Total"
        '
        'Valeur
        '
        Me.Valeur.AutoSize = True
        Me.Valeur.Location = New System.Drawing.Point(356, 225)
        Me.Valeur.Name = "Valeur"
        Me.Valeur.Size = New System.Drawing.Size(13, 13)
        Me.Valeur.TabIndex = 4
        Me.Valeur.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Valeur)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.miseazero)
        Me.Controls.Add(Me.add)
        Me.Controls.Add(Me.reduce)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents reduce As Button
    Friend WithEvents add As Button
    Friend WithEvents miseazero As Button
    Friend WithEvents Total As Label
    Friend WithEvents Valeur As Label
End Class
