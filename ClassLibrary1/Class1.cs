﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Counteur
    {
        private static int compteur = 0;

        public static int affichageC()
        {
            return Counteur.compteur;
        }
        public static void increC()
        {
            Counteur.compteur += 1;
        }
        public static void decreC()
        {
            Counteur.compteur -= 1;
        }
        public static void Razc()
        {
            Counteur.compteur = 0;
        }
    }
}
